/* Linux headers */
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>


/* CSP headers */
#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>
#include <csp/drivers/usart.h>
#include <csp/arch/csp_thread.h>

/* CSP ports definitions */

#define MY_ADDRESS 1
#define DEST_ADDRESS 2
#define POWER_HK_PORT 8
#define POWER_REQ_PORT 9

#define SERVER_TIDX 0
#define CLIENT_TIDX 1
#define USART_HANDLE 0




/* NanoPower Housekeeping Structure */

typedef struct __attribute__((packed)) {
uint16_t vboost[3]; //! Voltage of boost converters [mV] [PV1, PV2, PV3]
uint16_t vbatt; //! Voltage of battery [mV]
uint16_t curin[3]; //! Current in [mA]
uint16_t cursun; //! Current from boost converters [mA]
uint16_t cursys; //! Current out of battery [mA]
uint16_t reserved1; //! Reserved for future use
uint16_t curout[6]; //! Current out (switchable outputs) [mA]
uint8_t output[8]; //! Status of outputs**
uint16_t output_on_delta[8]; //! Time till power on** [s]
uint16_t output_off_delta[8]; //! Time till power off** [s]
uint16_t latchup[6]; //! Number of latch-ups
uint32_t wdt_i2c_time_left; //! Time left on I2C wdt [s]
uint32_t wdt_gnd_time_left; //! Time left on I2C wdt [s]
uint8_t wdt_csp_pings_left[2]; //! Pings left on CSP wdt
uint32_t counter_wdt_i2c; //! Number of WDT I2C reboots
uint32_t counter_wdt_gnd; //! Number of WDT GND reboots
uint32_t counter_wdt_csp[2]; //! Number of WDT CSP reboots
uint32_t counter_boot; //! Number of EPS reboots
int16_t temp[6]; //! Temperatures [degC] [0 = TEMP1, TEMP2, TEMP3, TEMP4, BP4a, BP4b]*
uint8_t bootcause; //! Cause of last EPS reset
uint8_t battmode; //! Mode for battery [0 = initial, 1 = undervoltage, 2 = nominal, 3 = batteryfull]
uint8_t pptmode; //! Mode of PPT tracker [1=MPPT, 2=FIXED]
uint16_t reserved2;
} eps_hk_t;

/* ========================================= */

/* Global variables */

eps_hk_t hk;
int setup = 0;
int running = 1;


/* ========================================= */


int main(int argc, char **argv) {
    csp_debug_toggle_level(CSP_PACKET);
    csp_debug_toggle_level(CSP_INFO);
    //csp_debug_toggle_level(CSP_PROTOCOL);



    struct usart_conf conf;

#if defined(CSP_POSIX)
// Arduino needs 9600 baud
    conf.device = argc != 2 ? "/dev/ttyACM0" : argv[1];
    conf.baudrate = 9600;
#endif

	/* Run USART init */
	usart_init(&conf);

    csp_buffer_init(10, 300);


    /* Setup CSP interface */
	static csp_iface_t csp_if_kiss;
	static csp_kiss_handle_t csp_kiss_driver;
	csp_kiss_init(&csp_if_kiss, &csp_kiss_driver, usart_putc, usart_insert, "KISS");
		
	/* Setup callback from USART RX to KISS RS */
	void my_usart_rx(uint8_t * buf, int len, void * pxTaskWoken) {
		csp_kiss_rx(&csp_if_kiss, buf, len, pxTaskWoken);
	}
	usart_set_callback(my_usart_rx);

while(1){
    running = 1;
    csp_init(MY_ADDRESS);
    csp_route_set(DEST_ADDRESS, &csp_if_kiss, CSP_NODE_MAC);
    csp_route_start_task(0, 0);

    csp_socket_t *socketin = csp_socket(CSP_SO_CRC32REQ);
    csp_conn_t *connin;
    csp_socket_t *socketout = csp_socket(CSP_SO_CRC32REQ);
    csp_conn_t *connout;
    csp_packet_t *hk_req;
    csp_packet_t *hk_data;

    hk_data = csp_buffer_get(sizeof(eps_hk_t));
    if( hk_data == NULL ) {
        fprintf(stderr, "Could not allocate memory for response packet!\n");
    }
   
    hk.vbatt = 0xEFAB;
    
    uint8_t *packet = (uint8_t *)&hk;
    hk_data->length = sizeof(hk);
    memcpy(hk_data->data, packet, sizeof(hk));
    
    csp_bind(socketin, POWER_REQ_PORT);
    csp_bind(socketout, POWER_HK_PORT);
    csp_listen(socketin, 5);
    
    printf("NanoPower HK Ready \n");

    while(running){
        if((connin = csp_accept(socketin, 1000)) != NULL){
            hk_req = csp_read(connin, 100);
            connout = csp_connect(1, DEST_ADDRESS, POWER_HK_PORT, 1000, CSP_O_CRC32);
            csp_send(connout, hk_data, 1000);
            csp_close(connin);
            csp_close(connout);
            running = 0;
        }
        /*
        struct timespec ts;
        ts.tv_sec = 0;
        ts.tv_nsec = 100000;
        nanosleep(&ts, NULL);
        */
    }
}
    return 0;

}

